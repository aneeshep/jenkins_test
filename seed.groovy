def imageMappings = [
    NC: [
        [
            prefix: "gstools",
            images: [
                "allmid",
                "attlogins",
                "attnologin",
                "attsudo",
                "cphalo",
                "eksh",
                "mcafee",
                "sact",
                "sensage",
                "sentinelone",
                "tsco",
                "uam",
                "uamroles",
                "xpw",
            ]
        ],
        [
            prefix: "sre",
            images: [
                "iproute2",
            ]
        ]
    ]
]

folder("images/nc-dockerfiles")
for (entry in imageMappings.NC) {
    for (image in entry.images) {
        folder("images/nc-dockerfiles/${entry.prefix}")
        pipelineJob("images/nc-dockerfiles/${entry.prefix}/${image}") {
            logRotator{
                daysToKeep(30)
            }
            configure {
                node -> node / 'properties' / 'jenkins.branch.RateLimitBranchProperty_-JobPropertyImpl'{
                    durationName 'hour'
                    count '3'
                }
            }
            triggers {
                gerritTrigger {
                    serverName('mtn5-gerrit')
                    gerritProjects {
                        gerritProject {
                            compareType('PLAIN')
                            pattern("nc-dockerfiles")
                            branches {
                                branch {
                                compareType("ANT")
                                pattern("**")
                                }
                            }
                            filePaths {
                                filePath {
                                  compareType('ANT')
                                  pattern("${entry.prefix}-${image}/**")
                                }
                                filePath {
                                  compareType('ANT')
                                  pattern("Makefile")
                                }
                            disableStrictForbiddenFileVerification(false)
                            }
                        }
                    }
                    triggerOnEvents {
                        patchsetCreated {
                           excludeDrafts(false)
                           excludeTrivialRebase(false)
                           excludeNoCodeChange(false)
                        }
                        changeMerged()
                        commentAddedContains {
                           commentAddedCommentContains('recheck')
                        }
                    }
                }

                definition {
                    cps {
                      script(readFileFromWorkspace("Jenkinsfile"))
                        sandbox(true)
                    }
                }
            }
        }
    }
}
